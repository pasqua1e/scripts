from pymongo import MongoClient # type: ignore

# Connect to MongoDB
client = MongoClient('mongodb://localhost:27017/')  # Update with your MongoDB connection string

# Access database
db = client['animal_db']  # Replace with your database name

# Access collection
collection = db['animal_tb']  # Replace with your collection name

# Data to insert
data = [
    {"id": 1, "name": "Lion", "type": "wild"},
    {"id": 2, "name": "Cow", "type": "domestic"},
    {"id": 3, "name": "Tiger", "type": "wild"}
]

# Insert data into collection
result = collection.insert_many(data)

#print("Data inserted successfully:", result.inserted_ids)

#admin user creation - used for db remote auth
db = client['admin']
db.command("createUser", "admin", pwd="admin", roles=[{"role": "userAdminAnyDatabase", "db": "admin"}, {"role": "readWriteAnyDatabase", "db": "admin"}])

#user login - used for app login
db = client['login']
users_collection = db['users'] 
user_document = { "username": "pasq", "password": "pasq" }
insert_result = users_collection.insert_one(user_document)
print("Inserted document ID:", insert_result.inserted_id)